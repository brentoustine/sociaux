const express=require('express');
const router=express.Router();
const authMiddleware=require('../middleware/authMiddleware');
const UserModel=require('../models/UserModel');
const PostModel=require('../models/PostModel');
const FollowerModel=require('../models/FollowerModel');


// CREATE A POST

router.post('/', authMiddleware, async(req, res) => {

	const {text, location, picUrl} = req.body;

	if (text.length < 1 ) return res.status(401).send('Text must be atleast 1 character');

		try {

			const newPost = {
				user: req.userId,
				text
			};

			if (location)newPost.location = location;
			if (picUrl)newPost.picUrl = picUrl;

			const post = await new PostModel(newPost).save();

		    return res.json(post);

		} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}

});


// GET ALL POSTS

router.get('/', authMiddleware, async(req, res)=> {

	try {

		const post = await PostModel.find()
		.sort({createdAt:-1})
		.populate("user")
		.populate("comments.user");

		return res.json(post);

	} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}

});

 //GET POST BY ID

router.get('/:postId', authMiddleware, async(req,res) => {

	try {

		const post = await PostModel.findById(req.params.postId)
		.populate("user")
		.populate("comments.user");

		if(!post){
			return res.status(404).send('Post not found');
		}

		return res.json(post);

	} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}

});


 //DELETE POST

router.delete('/:postId',authMiddleware,async(req,res) => {

	try {

		const {userId} = req;

		const {postId} = req.params;

	const post = await PostModel.findById(postId)
	if(!post) {
		return res.status(404).send('Post not found');
	}

	const user = await UserModel.findById(userId);

	if(post.user.toString()!==userId) {
		if(user.role === 'root'){
			await post.remove();
			return res.status(200).send('Post deleted Successfully!');
		} else {
			return res.status(401).send("Unauthorized");
		}
	}

	        await post.remove();
			return res.status(200).send('Post deleted Successfully!');

	} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}
});

// LIKE A POST

router.post('/like/postId', authMiddleware, async (req, res) => {

	try {
		const {postId} = req.params;
		const {userId} = req;

		const post = await PostModel.findById(postId);

		if (!post) {
			return res.status(404).send('No Post Found');
		}

		const isLiked = post.likes.filter(like => like.user.toString()===userId).length > 0;

		if (isLiked) {
			return res.status(401).semd('Post already liked');
		}

		await post.likes.unshift({user:userId});
		await post.save();

		return res.status(200).send('Post liked');

	} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}

});

// UNLIKE A POST

router.post('/unlike/postId', authMiddleware, async (req, res) => {

	try {
		const {postId} = req.params;
		const {userId} = req;

		const post = await PostModel.findById(postId);

		if (!post) {
			return res.status(404).send('No Post Found');
		}

		const isLiked = post.likes.filter(like => like.user.toString()===userId).length === 0;

		if (isLiked) {
			return res.status(401).semd('Post not liked before');
		}

		const index = post.likes.map(like => like.user.toString()).indexOf(userId);

		await post.likes.splice(index, 1);

		await post.save();

		return res.status(200).send('Post liked');

	} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}

});

//GET ALL LIKES

router.get('/like/:postId', async(req, res) => {
	try {

		const { postId } = req.params;

		const post = await PostModel.findById(postId)

		if (!post) {
			return res.status(404).send('No Post Found');
		}

	} catch (error) {
			console.error(error);
		return res.status(500).send('Server Error!');
		}

})


module.exports=router;